<?php

namespace Drupal\password_stats;

/**
 * Interface for password statistics.
 */
interface PasswordStatsInterface {

  /**
   * Returns the total number of hashed passwords.
   *
   * @param bool $includeInactive
   *   Count inactive accounts as well. By default only count active ones.
   */
  public function getTotalCount(bool $includeInactive = FALSE): int;

  /**
   * Returns the number of legacy password hashes.
   *
   * @param bool $includeInactive
   *   Count inactive accounts as well. By default only count active ones.
   */
  public function getLegacyCount(bool $includeInactive = FALSE): int;

  /**
   * Returns the number of password hashes matching the given prefixes.
   *
   * @param string[] $prefix
   *   A list of password hash prefixes.
   * @param bool $includeInactive
   *   Count inactive accounts as well. By default only count active ones.
   */
  public function getPrefixCount(array $prefix, bool $includeInactive = FALSE): int;

}
