<?php

namespace Drupal\password_stats\Commands;

use Drupal\password_stats\PasswordStatsInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush integration for password statistics.
 */
class PasswordStatsCommands extends DrushCommands {

  /**
   * Constructs a new password statistics drush command.
   */
  public function __construct(
    protected PasswordStatsInterface $passwordStats
  ) {}

  /**
   * Returns the total number of hashed passwords.
   *
   * @command password_stats:total
   *
   * @option include-inactive
   *   Count inactive accounts as well. By default only count active ones.
   *
   * @usage password_stats-total
   *   Returns the total number of hashed passwords.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   *
   * @return string
   */
  public function totalCount($options = ['include-inactive' => FALSE]) {
    return (string) $this->passwordStats->getTotalCount($options['include-inactive']);
  }

  /**
   * Returns the number of stored passwords hashed with legacy algorithms.
   *
   * @command password_stats:legacy
   *
   * @option include-inactive
   *   Count inactive accounts as well. By default only count active ones.
   *
   * @usage password_stats-legacy
   *   Returns the number of stored passwords hashed with legacy algorithms.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   *
   * @return string
   */
  public function legacyCount($options = ['include-inactive' => FALSE]) {
    return (string) $this->passwordStats->getLegacyCount($options['include-inactive']);
  }

}
