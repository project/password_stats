<?php

namespace Drupal\password_stats;

/**
 * Trait which implements common password stat methods.
 */
trait PasswordStatsTrait {

  /**
   * {@inheritdoc}
   */
  public function getLegacyCount($includeInactive = FALSE): int {
    $prefix = ['U$', '$S$', '$H$', '$P$'];
    return $this->getPrefixCount($prefix, $includeInactive);
  }

}
