<?php

namespace Drupal\password_stats;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;

/**
 * Implements password stats for core user entities stored in database.
 */
class PasswordStatsDatabase implements PasswordStatsInterface {

  use PasswordStatsTrait;

  /**
   * Constructs a password stats service for core user entities.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The entity type manager.
   */
  public function __construct(
    protected Connection $database
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getTotalCount(bool $includeInactive = FALSE): int {
    $result = $this->select($includeInactive)
      ->isNotNull('pass')
      ->countQuery()
      ->execute();
    return $result->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function getPrefixCount(array $prefix, bool $includeInactive = FALSE): int {
    $conditions = $this->database->condition('OR');
    foreach ($prefix as $pfx) {
      $conditions->condition('pass', $this->database->escapeLike($pfx) . '%', 'LIKE');
    }

    $result = $this->select($includeInactive)
      ->condition($conditions)
      ->countQuery()
      ->execute();
    return $result->fetchField();
  }

  /**
   * Returns a select statement with conditions applied.
   */
  protected function select(bool $includeInactive = FALSE): SelectInterface {
    $query = $this->database->select('users_field_data');

    if (!$includeInactive) {
      $query = $query->condition('status', 1);
    }

    return $query;
  }

}
