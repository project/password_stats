# Password Stats

This module provides a `drush` command and a status line in the requirements
table with information about stored password hashes.

Drupal uses the default PHP <code>password_hash()</code> and
<code>password_verify()</code> functions in order to store and verify passwords
securely since Drupal 10.1.x. User entities created with an older version
might still have hashes generated with the previous password hashing algorithm.

This module displays the total number of stored password hashes along with the
number of active users with password hashes generated prior to Drupal 10.1.0.

It also displays a recommendation to disable the [Password Compatibility] module
if no active users with passwords hashed by Drupal prior to 10.1.0 were found on
a site.

- For a full description of the module, visit the [project page].
- To submit bug reports and feature suggestions, or to track changes, use the
  [issue queue].

[Password Compatibility]: https://www.drupal.org/docs/core-modules-and-themes/core-modules/password-compatibility-module
[Project page]: https://www.drupal.org/project/password_stats
[issue queue]: https://www.drupal.org/project/issues/password_stats

## Table of contents

- Requirements
- Installation
- Usage
- Maintainers


## Requirements

Drupal 10.1.x or better. No other modules are required.


## Installation

- Install as you would normally install a contributed Drupal module. For further
  information, see _[Installing Drupal Modules]_.

[Installing Drupal Modules]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules


## Usage

### Drush

```
password_stats:
  password_stats:legacy Returns the number of stored passwords hashed with legacy algorithms.
  password_stats:total  Returns the total number of hashed passwords.
```


### Status report

Navigate to _Administration › Reports › Status report_ and review the
information under the _Password Compatibility_ heading.


## Maintainers


### Current maintainers

- [znerol](https://www.drupal.org/u/znerol)
